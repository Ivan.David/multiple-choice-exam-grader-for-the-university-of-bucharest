
1. the libraries required to run the project including the full version of each library
matplotlib==3.1.2
numpy==1.18.1
keras==2.3.1

2. how to run each scenario and where to look for the output file.
All the scenarios will run when the 'main_bun.py' script is run. In order to run the project on the files you will provide
us for testing, the 'additional_data' folder must be overridden with the appropriate folder structure
(i.e 1.scanned, 2.rotated+perspective, etc).

All the outputs will be found in the /output folder with the appropiate naming structure: name_surname_myGroup_taskNo#.txt