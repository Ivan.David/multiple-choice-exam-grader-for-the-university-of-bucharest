#!/usr/bin/env python
# -*- coding: utf-8 -*-
from classifier import Classifier
from image_reader import ImageReader
from scenario1 import Scenario1
from scenario2 import Scenario2
from scenario3 import Scenario3
from scenario4 import Scenario4
import os

def main():
    os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'
    scenario1_images = ImageReader.scenario1()
    scenario1_grades = Scenario1.run(scenario1_images, scenario=1)
    file = open('output/Ivan_Andrei-David_407_task1.txt', 'w+')
    for grade in scenario1_grades:
        file.write(grade)

    scenario2_images = ImageReader.scenario2()
    scenario2_grades = Scenario2.run(scenario2_images)
    file = open('output/Ivan_Andrei-David_407_task2.txt', 'w+')
    for grade in scenario2_grades:
        file.write(grade)

    scenario3_grades_part1, scenario3_grades_part2 = Scenario3.run()
    scenario3_grades = scenario3_grades_part1 + scenario3_grades_part2
    file = open('output/Ivan_Andrei-David_407_task3.txt', 'w+')
    for grade in scenario3_grades:
        file.write(grade)

    scenario4_images = ImageReader.scenario4()
    scenario4_grades = Scenario4.run(scenario4_images)
    file = open('output/Ivan_Andrei-David_407_task4.txt', 'w+')
    for grade in scenario4_grades:
        file.write(grade)

    #Classifier.pre_train_mnist()
    #Classifier.fine_tune()


if __name__ == "__main__":
    main()