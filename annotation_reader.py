class AnnotationReader:
    @staticmethod
    def scenario1(image_name):
        extension_index = image_name.find('.jpg')
        annotation = image_name[extension_index-2:extension_index]

        return annotation

