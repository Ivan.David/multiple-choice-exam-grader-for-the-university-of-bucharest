class Metrics:
    correctly_identified = 0
    incorrectly_identified = 0

    @staticmethod
    def grade_per_test(image_answers, ground_truth_answers):
        grade = 1.0
        for key in image_answers:
            if image_answers[key] == ground_truth_answers[key]:
                grade += 0.3

        return round(grade, 3)

    @staticmethod
    def compare_with(image_answers, ground_truth):
        for key in image_answers:
            if image_answers[key] == ground_truth[key]:
                Metrics.correctly_identified += 1
            else:
                Metrics.incorrectly_identified += 1

        return Metrics.correctly_identified, Metrics.incorrectly_identified