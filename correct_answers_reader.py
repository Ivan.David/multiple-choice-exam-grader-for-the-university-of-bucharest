import numpy as np
import os


class CorrectAnswersReader:
    @staticmethod
    def fromGroundTruth(annotation):
        file_name = ''
        if annotation[0] == 'F':
            file_name = 'Fizica_varianta'
        elif annotation[0] == 'I':
            file_name = 'Informatica_varianta'

        file_name += annotation[1]
        file_name += '.txt'

        base_path = 'ground-truth-correct-answers'
        correct_answers = np.loadtxt(os.path.join(base_path, file_name), dtype=str)
        correct_answers_dict = {}
        for array in correct_answers[1:-1]:
            correct_answers_dict[int(array[0])] = array[1]

        return correct_answers_dict
