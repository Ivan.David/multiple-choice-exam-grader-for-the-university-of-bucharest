import glob
import os

from Image import Image


class ImageReader:
    @staticmethod
    def fromTraining():
        base_folder = os.path.join('images')
        images_names = glob.glob(os.path.join(base_folder, 'image_*.jpg'))

        training_images = []
        for i in range(0, 150): # len(images_names)):
            print('Reading image %d...' % i)
            image = Image(images_names[i], 0, image=None)
            training_images.append(image)

        return training_images

    @staticmethod
    def fromTrainingRotation():
        base_folder = os.path.join('images')
        images_names = glob.glob(os.path.join(base_folder, 'rotation_*.jpg'))
        initial_index = 1
        final_index = 150

        training_images = []
        for i in range(0, 2): # len(images_names)):
            print('Reading image %d...' % i)
            image = Image(images_names[i], 0, None)
            training_images.append(image)

        return training_images

    @staticmethod
    def fromTrainingPerspective():
        base_folder = os.path.join('images')
        images_names = glob.glob(os.path.join(base_folder, 'perspective_*.jpg'))

        training_images = []
        for i in range(0, 2): # len(images_names)):
            print('Reading image %d...' % i)
            image = Image(images_names[i], 0, image=None)
            training_images.append(image)

        return training_images

    @staticmethod
    def scenario1():
        base_folder = os.path.join('additional_data', '1.scanned')
        images_names = glob.glob(os.path.join(base_folder, '*_scanned_*.jpg'))
        initial_index = 56
        final_index = 60

        scenario1_images = []
        for i in range(0, 5):# len(images_names)):
            print('Reading the %d image...' % i)
            image = Image(images_names[i], 1, image=None)
            scenario1_images.append(image)

        return scenario1_images

    @staticmethod
    def scenario2():
        base_folder = os.path.join('additional_data', '2.rotated+perspective')
        images_names = glob.glob(os.path.join(base_folder, '*_*_*.jpg'))

        scenario2_images = []
        for i in range(0, len(images_names)):
            print('Reading the %d image...' % i)
            image = Image(images_names[i], 1, image=None)
            scenario2_images.append(image)

        return scenario2_images

    @staticmethod
    def scenario3():
        base_folder = os.path.join('additional_data', '3.no_annotation')
        images_names = glob.glob(os.path.join(base_folder, '*.jpg'))

        scenario3_images = []
        for i in range(0, len(images_names)):
            print('Reading the %d image...' % i)
            image = Image(images_names[i], 1, image=None)
            scenario3_images.append(image)

        return scenario3_images

    @staticmethod
    def scenario4():
        base_folder = os.path.join('additional_data', '4.handwritten')
        images_names = glob.glob(os.path.join(base_folder, '*.jpg'))

        scenario4_images = []
        for i in range(0, len(images_names)):
            print('Reading the %d image...' % i)
            image = Image(images_names[i], 1, image=None)
            scenario4_images.append(image)

        return scenario4_images
