import cv2 as cv
import numpy as np


class TableDetector:
    @staticmethod
    def find_rows(grayscale_image, SHOW_INTERMEDIATE_RESULTS):
        NUM_OF_SECONDS = 0

        # find the edges on the Y axis
        # apply Sobel filter on the Y axis
        edges_y = cv.Sobel(grayscale_image, ddepth=cv.CV_64F, dx=0, dy=1)
        # normalize the output
        edges_y = np.abs(edges_y)
        edges_y = edges_y / edges_y.max()

        if SHOW_INTERMEDIATE_RESULTS:
            cv.imshow("edges_y", cv.resize(edges_y, (0, 0), fx=0.2, fy=0.2))
            cv.waitKey(NUM_OF_SECONDS)
            cv.destroyAllWindows()

            # convert to a black and white image
        _, edges_y_th = cv.threshold(edges_y, 0.4, 255, cv.THRESH_BINARY_INV)  # the second param is the threshold
        if SHOW_INTERMEDIATE_RESULTS:
            cv.imshow("edges_y", cv.resize(edges_y_th, (0, 0), fx=0.2, fy=0.2))
            cv.waitKey(NUM_OF_SECONDS)
            cv.destroyAllWindows()

        # create a mask for black pixels
        mask = (edges_y_th == 0) * 1
        # find the rows from mask
        all_lines = np.sum(mask, axis=1)
        all_lines = all_lines.argsort()

        num_lines = 60
        edges_y_th = np.dstack((edges_y_th, edges_y_th, edges_y_th))
        lines = []  # _ x
        for i in range(1, num_lines + 1):
            cv.line(edges_y_th, (0, all_lines[-i]), (grayscale_image.shape[1], all_lines[-i]), (0, 0, 255), 2)
            lines.append([(0, all_lines[-i]), (grayscale_image.shape[1], all_lines[-i])])

        if SHOW_INTERMEDIATE_RESULTS:
            cv.imshow("edges_y_th", cv.resize(edges_y_th, (0, 0), fx=0.2, fy=0.2))
            cv.waitKey(NUM_OF_SECONDS)
            cv.destroyAllWindows()

        # sort only on y
        lines.sort(key=lambda coords: coords[0][1])

        # luam liniile distincte cu un threshold de 30 de pixeli
        threshold_same_line = 30
        distict_lines = []
        distict_lines.append(lines[0])

        for line in lines:
            if line[0][1] - distict_lines[-1][0][1] > threshold_same_line:
                distict_lines.append(line)

                # take the last 16 lines
        correct_lines = distict_lines[-16:]
        color_image = np.dstack((grayscale_image, grayscale_image, grayscale_image))
        for line in correct_lines:
            cv.line(color_image, line[0], line[1], (255, 0, 0), 5)

        if SHOW_INTERMEDIATE_RESULTS:
            cv.imshow("Detected lines", cv.resize(color_image, (0, 0), fx=0.2, fy=0.2))
            cv.waitKey(NUM_OF_SECONDS)
            cv.destroyAllWindows()

        return correct_lines

    @staticmethod
    def find_columns(self, grayscale_image, SHOW_INTERMEDIATE_RESULTS):
        NUM_OF_SECONDS = 0
        # find the edges on the X axis
        edges_x = cv.Sobel(grayscale_image, ddepth=cv.CV_64F, dx=1, dy=0)
        # normalize the output
        edges_x = abs(edges_x)
        edges_x = edges_x / edges_x.max()
        if SHOW_INTERMEDIATE_RESULTS:
            cv.imshow("edges_x", cv.resize(edges_x, (0, 0), fx=0.2, fy=0.2))
            cv.waitKey(NUM_OF_SECONDS)
            cv.destroyAllWindows()

        # convert to a black and white image
        # tot ce e sub 0.2 face alb, restul face negru
        _, edges_x_th = cv.threshold(edges_x, 0.20, 255, cv.THRESH_BINARY_INV)  # the second param is the threshold
        if SHOW_INTERMEDIATE_RESULTS:
            cv.imshow("edges_x", cv.resize(edges_x_th, (0, 0), fx=0.2, fy=0.2))
            cv.waitKey(NUM_OF_SECONDS)
            cv.destroyAllWindows()

        # create a mask for black pixels
        # vrea sa inverseze valorile de alb si negru, cica o ajuta la calcule
        mask = (edges_x_th == 0) * 1

        # fac suma de Oy ca sa-mi dau seama unde am linie, acolo unde valoarea e cea mai mare, acolo am linie
        all_cols = np.sum(mask, axis=0)
        all_cols = all_cols.argsort()  # indecsii sortati crescator
        num_cols = 60
        edges_x_th = np.dstack((edges_x_th, edges_x_th, edges_x_th))
        cols = []  # _ x

        # ia liniile in care are cea mai mare'incredere', adica primele 60 cu cel mai mare nr de pixeli negri
        # in exemplele lui o sa fie fix aceeasi distanta intre linii, ca e din acelasi unghi si deci pot sa hardcodez
        # exact distanta dintre cele doua coloane
        for i in range(1, num_cols + 1):
            cv.line(edges_x_th, (all_cols[-i], 0), (all_cols[-i], grayscale_image.shape[0]), (0, 0, 255), 2)
            cols.append([(all_cols[-i], 0), (all_cols[-i], grayscale_image.shape[0])])

        if SHOW_INTERMEDIATE_RESULTS:
            cv.imshow("edges_x_th", cv.resize(edges_x_th, (0, 0), fx=0.2, fy=0.2))
            cv.waitKey(NUM_OF_SECONDS)
            cv.destroyAllWindows()

        # sort only on x0
        cols.sort(key=lambda coords: coords[0][0])
        threshold_same_column = 30
        distinct_cols = []
        distinct_cols.append(cols[0])

        for col in cols:
            if col[0][0] - distinct_cols[-1][0][0] > threshold_same_column:
                distinct_cols.append(col)

                # take the last 5 cols
        correct_cols = distinct_cols[-5:]
        color_image = np.dstack((grayscale_image, grayscale_image, grayscale_image))
        for col in correct_cols:
            cv.line(color_image, col[0], col[1], (255, 0, 0), 5)
        if SHOW_INTERMEDIATE_RESULTS:
            cv.imshow("Detected cols", cv.resize(color_image, (0, 0), fx=0.2, fy=0.2))
            cv.waitKey(NUM_OF_SECONDS)
            cv.destroyAllWindows()

        return correct_cols

    @staticmethod
    def find_table(self, grayscale_image):
        cols = self.find_columns(grayscale_image.copy())
        rows = self.find_rows(grayscale_image.copy())
        x_min = cols[0][0][0]
        x_max = cols[-1][1][0]
        y_min = rows[0][0][1]
        y_max = rows[-1][1][1]
        # y_max = ultima coloana, al 2-lea tuplu si a 2-a coordonata

        table = grayscale_image[y_min:y_max, x_min:x_max]
        image = np.dstack((grayscale_image, grayscale_image, grayscale_image))

        for i in range(5):
            cv.line(image, cols[i][0], cols[i][1], (255, 0, 0), 5)
        for i in range(16):
            cv.line(image, rows[i][0], rows[i][1], (0, 0, 255), 5)
        cv.imshow('image', cv.resize(image, (0, 0), fx=0.2, fy=0.2))
        cv.imshow('table', cv.resize(table, (0, 0), fx=0.2, fy=0.2))
        cv.waitKey(0)
        cv.destroyAllWindows()
        return table, [x_min, y_min, x_max, y_max], cols, rows

    @staticmethod
    def detect_subject_choice(self, choice_patch_grayscale):
        # apply findCountour function in order to extract all the countour in the image
        # take only the black (ish) pixels
        # alegem pixelii care sunt sub un treshold => negri
        # aplic uint8 pt ca initiat are doar True si False
        mask = np.uint8(choice_patch_grayscale < 50)
        # fct asta obtine contururile si noi o sa vrem sa obtinem bounding boxurile
        contours, hierarchy = cv.findContours(mask, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

        # obtinem bounding boxurile prin min si max pe x si y
        areas = []
        coords = []
        if len(contours) != 0:
            for idx_det in range(len(contours)):
                contour = contours[idx_det]
                contour = np.squeeze(contour)

                if contour.ndim == 1:
                    continue

                x_min = np.min(contour[:, 0])
                x_max = np.max(contour[:, 0])

                y_min = np.min(contour[:, 1])
                y_max = np.max(contour[:, 1])
                # calculez ariile bounding boxurilor
                areas.append(((x_max - x_min) * (y_max - y_min)))
                coords.append([(x_min, y_min), (x_max, y_max)])
        if len(areas) < 2:
            return None

        areas = np.array(areas)
        # iau bboxes cu ariile cele mai mari pt ca ma astept ca alea sa fie casutele
        indices = areas.argsort()
        bbox_1 = coords[indices[-1]]
        bbox_2 = coords[indices[-2]]
        return bbox_1, bbox_2
