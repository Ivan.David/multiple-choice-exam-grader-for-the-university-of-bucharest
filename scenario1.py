import cv2 as cv

from annotation_reader import AnnotationReader
from correct_answers_reader import CorrectAnswersReader
from line_detector import LineDetector
from metrics import Metrics


class Scenario1:
    @staticmethod
    def run(images, scenario):
        # images = ImageReader.fromTraining()
        metrics = Metrics()

        # todo:- loop aici si printat rezultatul in file
        grades = []
        for image in images:
            if scenario == 2 or scenario == 3:
                image.image = cv.bilateralFilter(image.image, 9, 80, 80)
            else:
                image.image = cv.bilateralFilter(image.image, 9, 20, 20)

            orig_h, orig_w, _ = image.image.shape
            lower_image = image.image[int(orig_h * 0.45): int(orig_h * 0.88)]
            grayscale_image = cv.cvtColor(lower_image, cv.COLOR_BGR2GRAY)
            # cv.imshow('image', cv.resize(grayscale_image, (0, 0), fx=0.2, fy=0.2))
            # cv.waitKey(0)
            # cv.destroyAllWindows()
            left_grayscale_image = grayscale_image[:, :int(0.5 * orig_w)]
            line_detector = LineDetector(image=left_grayscale_image, scenario=scenario)
            horizontal_lines, vertical_lines = line_detector.detect_lines()
            response_dict = LineDetector.find_cell_from_img(image=left_grayscale_image, vertical_lines=vertical_lines,
                                                            horizontal_lines=horizontal_lines, name=image.name,
                                                            dict_starting_index=0)

            # the right image is cut here in order to scrap the part with the id of the randomly assigned example
            # or take them bottom up
            # todo:- facut cumva mai elegant si scos un patch din care sa rezulte varianta aleasa
            right_grayscale_image = grayscale_image[600:, int(0.5 * orig_w):int(0.93 * orig_w)]
            line_detector = LineDetector(image=right_grayscale_image, scenario=scenario)
            horizontal_lines, vertical_lines = line_detector.detect_lines()
            response_dict_right = LineDetector.find_cell_from_img(image=right_grayscale_image,
                                                                  vertical_lines=vertical_lines,
                                                                  horizontal_lines=horizontal_lines, name=image.name,
                                                                  dict_starting_index=15)
            response_dict.update(response_dict_right)
            # the ground truth is what the student really marked as correct, not the actual real correct answers
            # subject, ground_truth_dict, result = GroundTruthReader.fromTraining(image.name)

            # UNCOMMENT FOR OFFICIAL VERSION !!!!
            if scenario == 1 or scenario == 2:
                image_annotation = AnnotationReader.scenario1(image.name)
            else:
                image_annotation = image.annotation

            correct_answers = CorrectAnswersReader.fromGroundTruth(image_annotation)
            grade = metrics.grade_per_test(response_dict, correct_answers)
            image_name = image.name.split('/')[-1]
            grades.append(f'{image_name}\t{grade:.2f}\n')

            # print('Grade for image %s' % image.name)
            # print('{0:.2f}'.format(metrics.grade_per_test(response_dict, correct_answers)))

            # ------------------- training data ----------------
            # if scenario == 2:
            #     image.name = image.name.replace('rotation', 'image')
            #     image.name = image.name.replace('perspective', 'image')
            #
            # subject, ground_truth_dict, result = GroundTruthReader.fromTraining(image.name)
            # metrics.compare_with(response_dict, ground_truth_dict)
            # print('Finish image %s' % image.name)
        return grades
        # print('Correctly identified:')
        # correctly_identified = (metrics.correctly_identified / (metrics.correctly_identified + metrics.incorrectly_identified)) * 100
        # print('%.2f' % correctly_identified)
        # #print('{0:.2f%}'.format(correctly_identified))
