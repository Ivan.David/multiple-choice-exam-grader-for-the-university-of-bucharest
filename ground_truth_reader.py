import numpy as np
import os


class GroundTruthReader:
    @staticmethod
    def fromTraining(image_name):
        # remove the .jpg extension
        image_name = image_name[:-4]
        ground_truth_content = np.loadtxt(os.path.join('%s.txt' % image_name), dtype=str)
        subject = ground_truth_content[0]

        ground_truth_dict = {}
        for array in ground_truth_content[1:-1]:
            ground_truth_dict[int(array[0])] = array[1]
        result = ground_truth_content[-1]

        return subject, ground_truth_dict, result
