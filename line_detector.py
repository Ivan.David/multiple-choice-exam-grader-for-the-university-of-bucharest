import math

import cv2 as cv
import numpy as np


class LineDetector:
    def __init__(self, image, scenario) -> None:
        super().__init__()
        self.image = image
        self.scenario = scenario

    def detect_lines(self):
        distinct_horizontal_lines = []
        distinct_vertical_lines = []

        edges = cv.Canny(self.image, 10, 100, None, 3)
        #era 5 x 5
        kernel = np.ones((5, 5), np.uint8)
        edges = cv.morphologyEx(edges, cv.MORPH_CLOSE, kernel)
        cdst = cv.cvtColor(edges, cv.COLOR_GRAY2BGR)
        hough_threshold = 150
        # resolution of rho in pixels
        rho = 1
        # resolution of theta in radians
        theta = np.pi / 2
        min_line_length = 600
        max_line_gap = 30

        if self.scenario == 2:
            hough_threshold = 140
            min_line_length = 50
            max_line_gap = 70

        lines = cv.HoughLinesP(edges, rho, theta, hough_threshold, None, min_line_length, max_line_gap)

        horizontal_lines = []
        vertical_lines = []

        if lines is None or len(lines) == 0:
            return [], []

        # x1, y1, x2, y2
        for line in lines:
            line = line[0]
            slope = math.atan2(line[3] - line[1], line[2] - line[0])
            if slope < 0:
                slope += np.pi

            # 1.483 = 85 deg in rad and 1.658 = 95 deg in rad
            # 0.087 = 5 deg in rad
            if 1.483 < slope < 1.658:
                vertical_lines.append(line)
            elif (self.scenario == 2 and slope < 0.187) or slope < 0.087:
                horizontal_lines.append(line)

        threshold_horizontal_line = 90
        horizontal_lines = sorted(horizontal_lines, key=lambda coords: coords[1])
        if len(horizontal_lines) > 0:
            distinct_horizontal_lines = [horizontal_lines[0]]

            for line in horizontal_lines:
                if line[1] - distinct_horizontal_lines[-1][1] > threshold_horizontal_line:
                    # print('Horizontal:')
                    # print(line[1] - distinct_horizontal_lines[-1][1])
                    distinct_horizontal_lines.append(line)

            distinct_horizontal_lines = distinct_horizontal_lines[-16:]
            for line in distinct_horizontal_lines:
                cv.line(cdst, (line[0], line[1]), (line[2], line[3]), (0, 0, 255), 3, cv.LINE_AA)

        if len(vertical_lines) > 0:
            threshold_vertical_line = 90
            vertical_lines = sorted(vertical_lines, key=lambda coords: coords[0])
            distinct_vertical_lines = [vertical_lines[0]]
            for line in vertical_lines:
                if line[0] - distinct_vertical_lines[-1][0] > threshold_vertical_line:
                    # print('Vertical:')
                    # print(line[0] - distinct_vertical_lines[-1][0])
                    distinct_vertical_lines.append(line)

            distinct_vertical_lines = distinct_vertical_lines[-5:]
            for line in distinct_vertical_lines:
                cv.line(cdst, (line[0], line[1]), (line[2], line[3]), (0, 0, 255), 3, cv.LINE_AA)

        # cv.imshow('image', cv.resize(cdst, (0, 0), fx=0.2, fy=0.2))
        # cv.waitKey(0)
        # cv.destroyAllWindows()

        if len(distinct_vertical_lines) > 0 and len(distinct_horizontal_lines) > 0:
            return distinct_horizontal_lines, distinct_vertical_lines
        else:
            return [], []

    @staticmethod
    def find_cell_from_img(image, vertical_lines, horizontal_lines, name, dict_starting_index):
        green_color = (0, 255, 0)
        red_color = (0, 0, 255)
        char_to_index = {'A': 0, 'B': 1, 'C': 2, 'D': 3}
        index_to_char = {0: 'A', 1: 'B', 2: 'C', 3: 'D'}

        # todo:- while debugging have it in 3D, in order to be able to print the colors of the x's
        image = np.dstack((image, image, image))

        response_dict = {}
        for i in range(len(horizontal_lines) - 1):
            minimum_patch_value_per_row = 260
            for j in range(len(vertical_lines) - 1):
                x_min = vertical_lines[j][0] + 25
                x_max = vertical_lines[j + 1][0] - 15
                y_min = horizontal_lines[i][1] + 25
                y_max = horizontal_lines[i + 1][1] - 15

                patch = image[y_min:y_max, x_min:x_max].copy()
                mean_patch_value = np.round(patch.mean())
                if mean_patch_value < minimum_patch_value_per_row:
                    minimum_patch_value_per_row = mean_patch_value

            for k in range(len(vertical_lines) - 1):
                x_min = vertical_lines[k][0] + 25
                x_max = vertical_lines[k + 1][0] - 15
                y_min = horizontal_lines[i][1] + 25
                y_max = horizontal_lines[i + 1][1] - 15

                patch = image[y_min:y_max, x_min:x_max].copy()
                mean_patch_value = np.round(patch.mean())

                if mean_patch_value == minimum_patch_value_per_row:
                    color = green_color
                    response_dict[i + 1 + dict_starting_index] = index_to_char[k]
                else:
                    color = red_color

                cv.rectangle(image, (x_min, y_min), (x_max, y_max), color=color, thickness=5)

        # cv.imshow(name, cv.resize(image, (0, 0), fx=0.3, fy=0.3))
        # cv.waitKey(0)
        # cv.destroyAllWindows()

        #print(response_dict)
        return response_dict

    @staticmethod
    def find_cell_from_gt(image, vertical_lines, horizontal_lines, ground_truth):
        green_color = (0, 255, 0)
        red_color = (0, 0, 255)
        char_to_index = {'A': 0, 'B': 1, 'C': 2, 'D': 3}

        # todo:- while debugging have it in 3D, in order to be able to print the colors of the x's
        image = np.dstack((image, image, image))

        response_dict = {}
        for i in range(len(horizontal_lines) - 1):
            for j in range(len(vertical_lines) - 1):
                x_min = vertical_lines[j][0] + 15
                x_max = vertical_lines[j + 1][0] - 5
                y_min = horizontal_lines[i][1] + 15
                y_max = horizontal_lines[i + 1][1] - 5

                patch = image[y_min:y_max, x_min:x_max].copy()

                if char_to_index[ground_truth[i][1]] == j:
                    color = green_color
                else:
                    color = red_color

                cv.rectangle(image, (x_min, y_min), (x_max, y_max), color=color, thickness=5)

        # cv.imshow('img', cv.resize(image, (0, 0), fx=0.3, fy=0.3))
        # cv.waitKey(0)
        # cv.destroyAllWindows()




