import cv2 as cv

from annotation_reader import AnnotationReader


class Image:
    def __init__(self, name, scenario, image, annotation=''):
        self.name = name
        if image is None:
            self.image = cv.imread(name)
        else:
            self.image = image

        if scenario == 1:
            self.annotation = AnnotationReader.scenario1(image_name=name)
        if scenario == 2:
            self.annotation = AnnotationReader.scenario1(image_name=name)
        if scenario == 3:
            self.annotation = annotation
