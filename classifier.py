import glob
import os

import cv2 as cv
import keras
import matplotlib.pyplot as plt
import numpy as np
from keras import backend as K
from keras import layers
from keras import models
from keras.datasets import mnist
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Dense, Dropout, Flatten
from keras.models import Sequential
from keras.preprocessing.image import ImageDataGenerator


class Classifier:
    @staticmethod
    def get_dataset():
        base_folder = os.path.join('dataset')

        x = []
        y = []
        for label in range(1, 5):
            label_folder = os.path.join(base_folder, str(label))
            images_names = glob.glob(os.path.join(label_folder, '*.png'))
            for image_name in images_names:
                image = cv.imread(image_name, flags=cv.IMREAD_GRAYSCALE)
                image = cv.resize(image, (80, 80), interpolation=cv.INTER_AREA)
                x.append(image)
                y.append(label)

        x = np.array(x)
        y = np.array(y)
        return x, y

    @staticmethod
    def pre_train_mnist():
        batch_size = 128
        num_classes = 10
        epochs = 12

        # input image dimensions
        img_rows, img_cols = 28, 28

        # the data, split between train and test sets
        (x_train, y_train), (x_test, y_test) = mnist.load_data()

        if K.image_data_format() == 'channels_first':
            x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
            x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
            input_shape = (1, img_rows, img_cols)
        else:
            x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
            x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
            input_shape = (img_rows, img_cols, 1)

        x_train = x_train.astype('float32')
        x_test = x_test.astype('float32')
        x_train /= 255
        x_test /= 255
        print('x_train shape:', x_train.shape)
        print(x_train.shape[0], 'train samples')
        print(x_test.shape[0], 'test samples')

        # convert class vectors to binary class matrices
        y_train = keras.utils.to_categorical(y_train, num_classes)
        y_test = keras.utils.to_categorical(y_test, num_classes)

        model = Sequential()
        model.add(Conv2D(32, kernel_size=(3, 3),
                         activation='relu',
                         input_shape=input_shape))
        model.add(Conv2D(64, (3, 3), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))
        model.add(Flatten())
        model.add(Dense(128, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(num_classes, activation='softmax'))

        model.compile(loss=keras.losses.categorical_crossentropy,
                      optimizer=keras.optimizers.Adadelta(),
                      metrics=['accuracy'])

        model.fit(x_train, y_train,
                  batch_size=batch_size,
                  epochs=epochs,
                  verbose=1,
                  validation_data=(x_test, y_test))
        score = model.evaluate(x_test, y_test, verbose=0)
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])

        model.save('pre-trained-mnist.h5')

    @staticmethod
    def fine_tune():
        pre_trained_model = keras.models.load_model('pre-trained-mnist.h5')
        pre_trained_model.layers.pop()
        pre_trained_model.summary()

        # Freeze the layers except the last 4 layers
        for layer in pre_trained_model.layers[:-5]:
            layer.trainable = False

        model = models.Sequential()
        model.add(pre_trained_model)
        model.add(layers.Dense(4, activation='softmax'))

        data_generator = ImageDataGenerator(rescale=1. / 255, rotation_range=10, validation_split=0.05)

        train_generator = data_generator.flow_from_directory(
            'dataset',
            target_size=(28, 28),
            batch_size=40,
            class_mode='categorical',
            subset='training',
            color_mode='grayscale')
        validation_generator = data_generator.flow_from_directory(
            'dataset',
            target_size=(28, 28),
            batch_size=20,
            class_mode='categorical', subset='validation',
            color_mode='grayscale')

        model.compile(loss=keras.losses.categorical_crossentropy,
                      optimizer=keras.optimizers.Adadelta(),
                      metrics=['acc'])
        history = model.fit_generator(
            train_generator,
            steps_per_epoch=train_generator.samples / train_generator.batch_size,
            epochs=14,
            validation_data=validation_generator,
            validation_steps=validation_generator.samples / validation_generator.batch_size,
            verbose=1)

        model.save('fine-tuned-model-more-data.h5')

        acc = history.history['acc']
        val_acc = history.history['val_acc']
        loss = history.history['loss']
        val_loss = history.history['val_loss']

        epochs = range(len(acc))

        plt.plot(epochs, acc, 'b', label='Training acc')
        plt.plot(epochs, val_acc, 'r', label='Validation acc')
        plt.title('Training and validation accuracy')
        plt.legend()

        plt.figure()

        plt.plot(epochs, loss, 'b', label='Training loss')
        plt.plot(epochs, val_loss, 'r', label='Validation loss')
        plt.title('Training and validation loss')
        plt.legend()

        plt.show()