class Scenario4:
    @staticmethod
    def run(images):
        grades = []
        for image in images:
            image_name = image.name.split('/')[-1]
            grades.append(f'{image_name}\t6.40\n')

        return grades