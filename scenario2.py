import cv2 as cv
import numpy as np

from Image import Image
from scenario1 import Scenario1


class Scenario2:
    @staticmethod
    def run(images, scenario=2):
        # images = ImageReader.scenario2()
        scenario2_images = []
        # images = ImageReader.fromTrainingPerspective()
        # images += ImageReader.fromTrainingRotation()
        template_image = cv.imread('template_img_12.jpg')
        template_image = cv.cvtColor(template_image, cv.COLOR_BGR2RGB)

        for image in images:
            rgb_image = cv.cvtColor(image.image, cv.COLOR_BGR2RGB)
            orb = cv.ORB_create(nfeatures=10000, patchSize=40)

            template_mask = np.zeros(template_image.shape[:2], dtype=np.uint8)
            orig_h, orig_w, _ = template_image.shape
            cv.rectangle(template_mask,
                         (int(0.1 * orig_w), int(0.37 * orig_h)), (int(0.99 * orig_w), int(0.91 * orig_h)), 255,
                         thickness=-1)
            masked_data_template = cv.bitwise_and(template_image, template_image, mask=template_mask)

            # cv.imshow("masked_data_template", cv.resize(masked_data_template, (0, 0), fx = 0.1, fy=0.1))
            # cv.waitKey(0)
            # cv.destroyAllWindows()

            # cv.imshow("edges_y", cv.resize(template_mask, (0, 0), fx=0.1, fy=0.1))
            # cv.waitKey(0)
            # cv.destroyAllWindows()

            # find something better
            orig_h, orig_w, _ = rgb_image.shape
            query_mask = np.zeros(rgb_image.shape[:2], dtype=np.uint8)
            cv.rectangle(query_mask,
                         (int(0.12 * orig_w), int(0.28 * orig_h)), (int(0.89 * orig_w), int(0.72 * orig_h)), 255,
                         thickness=-1)
            masked_data = cv.bitwise_and(rgb_image, rgb_image, mask=query_mask)

            # cv.imshow("masked_data_rgb",cv.resize(masked_data, (0, 0), fx = 0.2, fy=0.2))
            # cv.waitKey(0)
            # cv.destroyAllWindows()
            # lower_image = image.image[int(orig_h * 0.45): int(orig_h * 0.78)]

            kp1, des1 = orb.detectAndCompute(template_image, template_mask)
            # print('(1) num of keypoints =', len(kp1))
            # print('(1) num of descriptors =', len(des1))
            # print('(1) the dimension of one descriptor is', len(des1[1]))
            # print('(1) the second descriptor is', des1[1])
            kp2, des2 = orb.detectAndCompute(rgb_image, query_mask)
            # print('(2) num of keypoints =', len(kp2))
            # print('(2) num of descriptors =', len(des2))
            # print('(2) the dimension of one descriptor is', len(des2[1]))
            # print('(2) the second descriptor is', des1[2])

            # drawing_img1 = np.copy(template_image)
            # cv.drawKeypoints(template_image, kp1[:500], drawing_img1, flags=cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
            # drawing_img2 = np.copy(rgb_image)
            # cv.drawKeypoints(rgb_image, kp2[:500], drawing_img2, flags=cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
            #
            # plt.figure(figsize=(14, 14))
            # plt.subplot(1, 2, 1)
            # plt.imshow(drawing_img1)
            # plt.subplot(1, 2, 2)
            # plt.imshow(drawing_img2)
            # plt.show()

            # create BFMatcher object
            # matcher takes normType, which is set to cv2.NORM_L2 for SIFT and SURF, cv2.NORM_HAMMING for ORB, FAST and BRIEF
            bf = cv.BFMatcher(cv.NORM_HAMMING, crossCheck=True)

            # Match descriptors.
            matches = bf.match(des2, des1)  # query_image, train_image
            print(len(matches))
            # Sort them in the order of their distance.
            matches = sorted(matches, key=lambda x: x.distance)

            # dist = [m.distance for m in matches]
            # thres_dist = (sum(dist) / len(dist))
            #
            # # keep only the reasonable matches
            sel_matches = matches[:int(len(matches) / 4)]

            # Draw first 150 matches.
            # plt.figure(figsize=(14, 14))
            # img3 = cv.drawMatches(rgb_image, kp2, template_image, kp1, sel_matches, None,
            #                       flags=cv.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
            # plt.imshow(img3)
            # plt.show()

            # plt.figure(figsize=(14, 14))
            # plt.subplot(1, 2, 1)
            # plt.imshow(template_image)
            # plt.subplot(1, 2, 2)
            # plt.imshow(rgb_image)
            # plt.show()

            # compute the homography matrix
            points_template = np.zeros((len(sel_matches), 2), dtype=np.float32)
            points_query = np.zeros((len(sel_matches), 2), dtype=np.float32)

            for i, m in enumerate(sel_matches):
                points_template[i, :] = kp1[m.trainIdx].pt
                points_query[i, :] = kp2[m.queryIdx].pt

            # print(points_template.shape)
            # print(points_query.shape)

            H, mask = cv.findHomography(points_query, points_template, cv.RANSAC, 15.0)
            height, width, _ = template_image.shape
            aligned_image = cv.warpPerspective(rgb_image, H, (width, height), flags=cv.INTER_NEAREST)
            # plt.figure(figsize=(10, 10))
            # fig, ax1 = plt.subplots(1, 1)
            # ax1.set_title(image.name)
            # plt.imshow(aligned_image)
            # plt.show()
            scenario2_images.append(
                Image(name=image.name, image=aligned_image, scenario=scenario, annotation=image.annotation))

            # ----------- scenario 1 ---------------
        return Scenario1.run(scenario2_images, scenario=scenario)
